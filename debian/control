Source: libconfig-properties-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Nick Morrott <knowledgejunkie@gmail.com>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libconfig-properties-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libconfig-properties-perl.git
Homepage: https://metacpan.org/release/Config-Properties
Testsuite: autopkgtest-pkg-perl

Package: libconfig-properties-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Multi-Arch: foreign
Description: Perl module to read and write Java-style property files
 Config::Properties is a near implementation of the java.util.Properties API.
 It is designed to allow easy reading, writing and manipulation of Java-style
 property files in Perl applications.
 .
 The format of a Java-style property file is that of a key-value pair
 separated by either whitespace, the colon (:) character, or the equals (=)
 character. Whitespace before the key and on either side of the separator is
 ignored. Lines that begin with either a hash (#) or a bang (!) are considered
 comment lines and ignored.
